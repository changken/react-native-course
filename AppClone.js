import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { View } from 'react-native';
import { Icon, Fab } from 'native-base';

import Click from './src/Click';
import ProductList from './src/product/ProductList';
import styles from './styles';
import Flexbox from './src/product/Flexbox';

export default function App() {
  const [count, setCount] = useState(10);
  const [modelVisible, setModalVisible] = useState(true);

  const toggleVisible = () => {
    // setModalVisible(!modelVisible);
    setModalVisible((oldModalVisible) => !oldModalVisible);
  };

  return (
    <View style={styles.container}>
      {/* <Text style={styles.text}>
        Open up App.js to start working on your app!
      </Text> */}
      {/* <Click count={count} setCount={(value) => setCount(value)} /> */}
      <ProductList visible={modelVisible} toggleVisible={toggleVisible} />
      {/*<Flexbox />*/}
      <StatusBar style="auto" />

      <Fab onPress={toggleVisible}>
        <Icon ios="ios-add" android="md-add" />
      </Fab>
    </View>
  );
}
