import React, { useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import ProductList from './src/product/ProductList';
import PersonList from './src/PersonList';
import SignUp from './src/account/SignUp';
import SignIn from './src/account/SignIn';
import SignOut from './src/account/SignOut';
import ApplyForm from './src/ApplyForm';
import { AuthContext } from './src/account/AuthContext';
import ImageUpload from './src/storage/ImageUpload';

const Tab = createBottomTabNavigator();

function App() {
  const [isSignedIn, setIsSignedIn] = useState(false);

  return (
    <NavigationContainer>
      <AuthContext.Provider
        value={{ isSignedIn: isSignedIn, setStatus: setIsSignedIn }}
      >
        <Tab.Navigator>
          {isSignedIn ? (
            <>
              <Tab.Screen name="SignOut" component={SignOut} />
              <Tab.Screen name="ProductList" component={ProductList} />
              <Tab.Screen name="PersonList" component={PersonList} />
              <Tab.Screen name="ApplyForm" component={ApplyForm} />
              <Tab.Screen name="ImageUpload" component={ImageUpload} />
            </>
          ) : (
            <>
              <Tab.Screen name="Signup" component={SignUp} />
              <Tab.Screen name="SignIn" component={SignIn} />
            </>
          )}
        </Tab.Navigator>
      </AuthContext.Provider>
    </NavigationContainer>
  );
}

export default App;
