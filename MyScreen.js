import React from 'react';
import { View, Text, Button } from 'react-native';
import { useNavigation } from '@react-navigation/native';
//import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
//const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

function ScreenA(props) {
  const navigation = useNavigation();
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>Screen A!</Text>
      <Button
        title="go to screen b"
        onPress={() => navigation.navigate('ScreenB')}
      />
    </View>
  );
}

function ScreenB(props) {
  const navigation = useNavigation();
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>Screen B!</Text>
      <Button
        title="go to screen a"
        onPress={() => navigation.navigate('ScreenA')}
      />
    </View>
  );
}

function MyScreen(props) {
  return (
    <Drawer.Navigator>
      <Drawer.Screen name="ScreenA" component={ScreenA} />
      <Drawer.Screen name="ScreenB" component={ScreenB} />
    </Drawer.Navigator>
  );
}

export default MyScreen;
