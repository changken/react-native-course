import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
//import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import HomeScreen from './HomeScreen';
import DetailsScreen from './DetailsScreen';
import MyScreen from './MyScreen';

//const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

function App() {
  return (
    <NavigationContainer>
      <Tab.Navigator>
        <Tab.Screen
          name="Details"
          component={DetailsScreen}
          initialParams={{ title: '104人力銀行' }}
          options={{
            title: '104奴隸銀行',
            headerStyle: {
              backgroundColor: '#00cc99',
            },
          }}
        />
        <Tab.Screen
          name="Home"
          component={HomeScreen}
          initialParams={{ count: 10 }}
        />
        <Tab.Screen name="MyScreen" component={MyScreen} />
      </Tab.Navigator>
    </NavigationContainer>
  );
}

export default App;
