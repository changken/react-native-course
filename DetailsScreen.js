import React from 'react';
import { View, Text, Button } from 'react-native';
//import { useNavigation } from '@react-navigation/native';

function DetailsScreen({
  route: {
    params: { title },
  },
}) {
  //const navigation = useNavigation();
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Details Screen {title}</Text>
      {/*<Button title="go to home" onPress={() => navigation.navigate('Home')} />*/}
    </View>
  );
}

export default DetailsScreen;
