import React from 'react';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import reducer from './src/store/reducer';
import TodoList from './src/components/TodoList';

const store = createStore(reducer);

export default function App() {
  return (
    // Provider管理store
    <Provider store={store}>
      <TodoList />
    </Provider>
  );
}
