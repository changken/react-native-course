import React, { useState, useEffect } from 'react';
import { StyleSheet, Alert, Button, View } from 'react-native';

export default function Click({ count, setCount }) {
  //let count = 0;
  //   const [count, setCount] = useState(0);
  let countString = 'count: ' + count;

  function handleClick() {
    //count++;
    setCount(count + 1);
    Alert.alert('count: ' + count);
  }

  //空陣列 只執行一次，沒有空陣列，當state variable變動就執行
  useEffect(() => {
    Alert.alert('start');
    return () => {
      Alert.alert('end');
    };
  }, []);

  //陣列裡有變數，變數變動就執行
  useEffect(() => {
    Alert.alert('useEffect: ' + count);
  }, [count]);

  //   let btnList = [1, 2, 3, 4, 5, 6, 7, 8].map((el) => (
  //     <Button
  //       title={el.toString()}
  //       onPress={() => Alert.alert('No. ' + el)}
  //       key={el}
  //       style={{ marginBottom: 10 }}
  //     />
  //   ));

  return (
    <View style={styles.buttonArea}>
      {/* {btnList} */}
      <Button title={countString} onPress={handleClick} />
      {/* <Button title={countString} onPress={() => setCount(count + 1)} /> */}
    </View>
  );
}

const styles = StyleSheet.create({
  buttonArea: {
    fontSize: 22,
    backgroundColor: '#00cc99',
    color: '#fff',
    padding: 20,
    borderRadius: 5,
  },
});
