import React, { useState, useEffect, useContext } from 'react';
import { Button, View, Text, TextInput, Image, StyleSheet } from 'react-native';
import * as firebase from 'firebase';
import * as FirebaseCore from 'expo-firebase-core';
import * as SecureStore from 'expo-secure-store';

import styles from '../styles';
import { AuthContext } from './AuthContext';

export default function SignIn() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [message, setMessage] = useState('');
  const authContext = useContext(AuthContext);

  if (!firebase.apps.length) {
    firebase.initializeApp(FirebaseCore.DEFAULT_WEB_APP_OPTIONS);
  }

  async function signIn() {
    try {
      const res = firebase.auth().signInWithEmailAndPassword(email, password);
      console.log('User login successfully!');
      setEmail('');
      setPassword('');
      setMessage('');
      const loginString = JSON.stringify({ email, password });
      await SecureStore.setItemAsync('login', loginString);
      authContext.setStatus(true);
    } catch (error) {
      setMessage(error.message);
    }
  }

  async function getAccount() {
    try {
      console.log('getAccount');
      setMessage('getting username');
      const loginString = await SecureStore.getItemAsync('login');
      const login = JSON.parse(loginString);
      setEmail(login.email);
      setPassword(login.password);
      setMessage('');
    } catch (error) {
      setMessage(error.message);
    }
  }

  useEffect(() => {
    getAccount();
  }, []);

  return (
    <View style={styles.form}>
      <Image
        height="50"
        width="50"
        source={require('../../image/user.png')}
        style={styles.signInIcon}
      />
      <TextInput
        style={styles.inputStyle}
        placeholder="電子信箱"
        value={email}
        onChangeText={text => setEmail(text)}
      />
      <TextInput
        style={styles.inputStyle}
        placeholder="密碼"
        value={password}
        onChangeText={text => setPassword(text)}
        maxLength={15}
        secureTextEntry={true}
      />
      <Button title="登入" onPress={signIn} />
      <Text>{message}</Text>
      <Text>尚未註冊，我要註冊</Text>
    </View>
  );
}
