export const axios_config = {
  headers: {
    Authorization: 'Bearer keyFvNJf2RMJwd3j0',
    'Context-Type': 'Application/json',
  },
};

export const base_url =
  'https://api.airtable.com/v0/appTpdQpPoXDMCKGh/Table%201';

export const usersTableUrl =
  'https://api.airtable.com/v0/appTpdQpPoXDMCKGh/users';

export const positionsTableUrl =
  'https://api.airtable.com/v0/appTpdQpPoXDMCKGh/positions';
