import React, { useState } from 'react';
import { Modal, TextInput, Button, StyleSheet, View } from 'react-native';

export default function Popup(props) {
  const [name, setName] = useState('');
  const [price, setPrice] = useState(0);

  const handleClick = () => {
    props.update({ name, price });
    props.toggleVisible();
  };

  const handleVisible = () => {
    props.toggleVisible();
  };

  return (
    <Modal
      visible={props.visible}
      onRequestClose={() => {
        console.log('closed');
      }}
    >
      <View style={styles.container}>
        <TextInput
          placeholder="產品名稱"
          onChangeText={(text) => setName(text)}
          value={name}
          style={styles.item}
        />
        <TextInput
          placeholder="產品價格"
          onChangeText={(text) => setPrice(parseInt(text))}
          value={price}
          style={styles.item}
        />
        <Button title="送出" onPress={handleClick} style={styles.item} />
        <Button title="關閉" onPress={handleVisible} style={styles.item} />
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '80%',
    height: 'auto',
    backgroundColor: '#00cc99',
    marginLeft: 'auto',
    marginRight: 'auto',
    padding: 10,
  },
  item: {
    fontSize: 22,
    color: '#fff',
    marginBottom: 10,
  },
});
