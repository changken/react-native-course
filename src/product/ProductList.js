import React, { useState, useEffect } from 'react';
import {
  TouchableOpacity,
  FlatList,
  View,
  Text,
  StyleSheet,
  YellowBox,
  StatusBar,
  ActivityIndicator,
} from 'react-native';
import * as firebase from 'firebase';
import firestore from 'firebase/firestore';
import * as FirebaseCore from 'expo-firebase-core';
import { Fab, Icon } from 'native-base';

import ProductAdd from './ProductAdd';
import Popup from './Popup';

const data = [
  {
    name: 'iphone se 2',
    price: 2000,
  },
  {
    name: 'iphone 11',
    price: 22000,
  },
  {
    name: 'samsung tab a 10.1',
    price: 6800,
  },
];

export default function ProductList(props) {
  YellowBox.ignoreWarnings(['Setting a timer']);

  if (!firebase.apps.length) {
    firebase.initializeApp(FirebaseCore.DEFAULT_WEB_APP_OPTIONS);
  }

  const db = firebase.firestore();

  const readData = async () => {
    setIsLoading(true);
    const newProducts = [];

    try {
      let productRef = db.collection('product').orderBy('price', 'desc');
      const querySnapshot = await productRef.get();

      querySnapshot.forEach(doc => {
        newProducts.push({
          id: doc.id,
          name: doc.data().name,
          price: doc.data().price,
        });
      });
      setProducts(newProducts);
    } catch (e) {
      console.log(e);
    } finally {
      setIsLoading(false);
    }
  };

  const updateData = async id => {
    const existProduct = products.find(value => value.id === id);
    setEditProduct(existProduct);
    toggleModalVisible();
  };

  const deleteData = async id => {
    try {
      await db.collection('product').doc(id).delete();
      setProducts(oldProducts => oldProducts.filter(value => value.id !== id));
      console.log('successful delete');
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    readData();
  }, [modalVisible]);

  useEffect(() => {
    readData();
  }, []);

  const [selected, setSelected] = useState(null);
  const [products, setProducts] = useState(data);
  const [isLoading, setIsLoading] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [editProduct, setEditProduct] = useState({});

  /*const Item = ({ name, price, index }) => {
    const backgroundColor = index === selected ? '#f9c2ff' : '#00ffff';
    return (
      <TouchableOpacity
        style={[styles.items, { backgroundColor }]}
        onPress={() => setSelected(index)}
      >
        <Text style={styles.title}>{name}</Text>
        <Text style={styles.title}>{price}</Text>
      </TouchableOpacity>
    );
  };*/

  /*const renderItem = ({ item, index }) => {
    return <Item {...item} index={index} />;
  };*/

  const add = newProduct => {
    setProducts(oldProducts => [...oldProducts, newProduct]);
  };

  const update = (id, editProduct) => {
    setProducts(oldProducts => [
      ...oldProducts.filter(value => value.id !== id),
      editProduct,
    ]);
  };

  const toggleModalVisible = () => {
    setModalVisible(oldModalVisible => !oldModalVisible);
  };

  const renderItem = ({ item: { id, name, price }, index }) => {
    const backgroundColor = index === selected ? '#f9c2ff' : '#00ffff';
    return (
      <TouchableOpacity
        style={[styles.items, { backgroundColor }]}
        onPress={() => {
          // deleteData(id);
          updateData(id);
          setSelected(index);
        }}
      >
        <Text style={styles.title}>{name}</Text>
        <Text style={styles.title}>{price}</Text>
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      <FlatList
        data={products}
        renderItem={renderItem}
        keyExtractor={item => item.name}
        style={styles.flatList}
      />
      <ActivityIndicator color="red" size="large" animating={isLoading} />
      <ProductAdd
        add={add}
        update={update}
        modalVisible={modalVisible}
        toggleModalVisible={toggleModalVisible}
        editProduct={editProduct}
        setEditProduct={setEditProduct}
      />
      <Fab onPress={() => toggleModalVisible()}>
        <Icon ios="ios-add" android="md-add" />
      </Fab>
      {/* <Popup
        visible={props.visible}
        toggleVisible={props.toggleVisible}
        update={update}
      /> */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: StatusBar.currentHeight,
    width: '80%',
    marginRight: 'auto',
    marginLeft: 'auto',
  },
  flatList: {
    flexGrow: 0,
  },
  items: {
    borderRadius: 5,
    backgroundColor: '#00cc99',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
    marginBottom: 10,
  },
  title: {
    color: '#fff',
    fontSize: 20,
    padding: 5,
    marginLeft: 10,
  },
});
