import React, { useState, useEffect } from 'react';
import { View, TextInput, Button, StyleSheet, Modal } from 'react-native';
import * as firebase from 'firebase';
import firestore from 'firebase/firestore';
import * as FirebaseCore from 'expo-firebase-core';

const ProductAdd = props => {
  if (!firebase.apps.length) {
    firebase.initializeApp(FirebaseCore.DEFAULT_WEB_APP_OPTIONS);
  }

  const db = firebase.firestore();

  const [name, setName] = useState('');
  const [price, setPrice] = useState('');

  const update = async () => {
    let docRef;
    if (props.editProduct.id) {
      docRef = await db
        .collection('product')
        .doc(props.editProduct.id)
        .set({
          name,
          price: parseInt(price),
        });
      props.update(props.editProduct.id, { name, price });
    } else {
      docRef = await db.collection('product').add({
        name,
        price: parseInt(price),
      });
      console.log(docRef.id);
      props.add({ name, price });
    }
    setName('');
    setPrice(0);

    props.toggleModalVisible();
  };

  useEffect(() => {
    if (props.editProduct.id) {
      setName(props.editProduct.name);
      setPrice(props.editProduct.price + '');
    }
  }, [props.modalVisible]);

  return (
    <Modal visible={props.modalVisible}>
      <View style={styles.container}>
        <TextInput
          placeholder="產品名稱"
          value={name}
          onChangeText={text => setName(text)}
          style={styles.textInput}
        />
        <TextInput
          placeholder="產品價格"
          value={price}
          onChangeText={text => setPrice(text)}
          style={styles.textInput}
        />
        <Button title="新增" onPress={update} />
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '80%',
    marginRight: 'auto',
    marginLeft: 'auto',
  },
  textInput: {
    fontSize: 20,
  },
});

export default ProductAdd;
