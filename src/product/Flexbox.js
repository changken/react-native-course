import React from 'react';
import { StyleSheet, View, Text } from 'react-native';

const Flexbox = () => {
  return (
    <View style={styles.wrap}>
      <Text style={styles.title}>幹 真大</Text>
      <Text style={styles.items}>32歲</Text>
      <Text style={styles.items}>投手</Text>
      <Text style={styles.title}>吳 短更</Text>
      <Text style={styles.items}>32歲</Text>
      <Text style={styles.items}>捕手</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  wrap: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'flex-start',
    flexWrap: 'wrap',
    padding: 5,
    backgroundColor: '#00cc99',
    width: '80%',
    height: '30%',
  },
  title: {
    flex: 4,
    backgroundColor: 'pink',
    fontSize: 28,
    flexBasis: '50%',
  },
  items: {
    flex: 1,
    backgroundColor: 'pink',
    fontSize: 28,
    flexBasis: '25%',
  },
});

export default Flexbox;
