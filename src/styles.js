import { StyleSheet, StatusBar } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ddd',
    marginTop: StatusBar.currentHeight || 0,
  },
  text: {
    color: '#fff',
    marginBottom: 10,
    backgroundColor: '#00cc99',
    padding: 10,
    borderRadius: 5,
  },
  form: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    padding: 35,
    marginTop: StatusBar.currentHeight || 0,
  },
  inputStyle: {
    width: '100%',
    marginBottom: 15,
    paddingBottom: 15,
    alignSelf: 'center',
    borderColor: '#ccc',
    borderBottomWidth: 1,
  },
  signInIcon: {
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  logo: {
    width: 305,
    height: 159,
    marginBottom: 20,
  },
});

export default styles;
