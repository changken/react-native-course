import React, { useState, useEffect, useContext } from 'react';
import {
  FlatList,
  View,
  Text,
  StyleSheet,
  StatusBar,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import { Icon, Fab } from 'native-base';
import axios from 'axios';

import { axios_config, base_url } from './config';
import PersonAdd from './PersonAdd';
import { AuthContext } from './account/AuthContext';

export default function PersonList() {
  const url = base_url + '?maxRecords=30&view=Grid%20view';

  const fetchData = async () => {
    const result = await axios.get(url, axios_config);
    setPersons(result.data.records);
    setIsLoading(false);
  };

  const renderItem = ({ item, index }) => {
    const backgroundColor =
      selectedId === item.id ? '#f9c2ff' : styles.item.backgroundColor;
    return (
      <TouchableOpacity
        style={[styles.item, { backgroundColor }]}
        onPress={() => update(item.id, index)}
      >
        <Text style={styles.text}>{index}</Text>
        <Text style={styles.text}>{item.fields.Name}</Text>
        <Text style={styles.text}>
          {item.fields.CityName.map(el => el + ',')}
        </Text>
        <Text style={styles.text}>{item.fields.Age}</Text>
      </TouchableOpacity>
    );
  };

  const [persons, setPersons] = useState({
    fields: [
      { Name: 'Ben', CityName: 'Taipei', Age: 16 },
      { Name: 'Cathy', CityName: 'Taipei', Age: 26 },
    ],
  });
  const [person, setPerson] = useState({});
  const [isLoading, setIsLoading] = useState(true);
  const [modalVisible, setModalVisible] = useState(false);
  const [selectedId, setSelectedId] = useState();
  const authContext = useContext(AuthContext);

  useEffect(() => {
    console.log(`isSignedIn in PersonList: ${authContext.isSignedIn}`);
    fetchData();
  }, [modalVisible]);

  const toggleModalVisible = () => {
    setModalVisible(oldModalVisible => !oldModalVisible);
  };

  const add = () => {
    setPerson({
      Name: '',
      CityName: '',
      Age: '',
    });
    setSelectedId('');
    toggleModalVisible();
  };

  const update = (id, index) => {
    setPerson({
      Name: persons[index].fields.Name,
      CityName: persons[index].fields.CityName.join(','),
      Age: persons[index].fields.Age,
    });
    setSelectedId(id);
    toggleModalVisible();
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Person List</Text>
      <ActivityIndicator color="red" size="large" animating={isLoading} />
      <FlatList
        data={persons}
        renderItem={renderItem}
        keyExtractor={(item, index) => '' + index}
        style={styles.flatList}
      ></FlatList>
      <PersonAdd
        toggleModalVisible={toggleModalVisible}
        modalVisible={modalVisible}
        person={person}
        id={selectedId}
      />
      <Fab onPress={add}>
        <Icon ios="ios-add" android="md-add"></Icon>
      </Fab>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingTop: StatusBar.currentHeight,
    flex: 1,
    justifyContent: 'center',
  },
  title: {
    fontSize: 44,
    alignSelf: 'center',
  },
  flatList: {
    flexGrow: 0,
  },
  item: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'pink',
  },
  text: {
    padding: 10,
    backgroundColor: 'dodgerblue',
    color: '#fff',
  },
});
