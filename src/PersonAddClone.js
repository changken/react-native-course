import React, { useState } from 'react';
import { Modal, TextInput, Button, View, StyleSheet } from 'react-native';
import axios from 'axios';

import { axios_config, base_url } from './config';

function PersonAdd(props) {
  const [name, setName] = useState('');
  const [cityName, setCityName] = useState('');
  const [age, setAge] = useState(0);

  const sendData = async () => {
    let cityArray = [];
    if (cityName.includes(',')) {
      cityArray = cityName.split(',');
    } else {
      cityArray = [...cityArray, cityName];
    }

    const newPerson = {
      fields: {
        Name: name,
        City: cityArray,
        Age: parseInt(age),
      },
      typecast: true,
    };

    try {
      const result = await axios.post(base_url, newPerson, axios_config);
      //console.log(result);
      props.toggleModalVisible();
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <Modal visible={props.modalVisible}>
      <View style={styles.container}>
        <TextInput
          placeholder="大名"
          value={name}
          onChangeText={text => setName(text)}
          style={styles.textInput}
        />
        <TextInput
          placeholder="城市名稱(逗號分隔)"
          value={cityName}
          onChangeText={text => setCityName(text)}
          style={styles.textInput}
        />
        <TextInput
          placeholder="年齡"
          value={age + ''}
          onChangeText={text => setAge(text)}
          style={styles.textInput}
        />
        <Button title="新增" onPress={sendData} />
        <Button title="Close" onPress={props.toggleModalVisible} />
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingLeft: 10,
    paddingRight: 10,
  },
  textInput: {
    fontSize: 20,
  },
});

export default PersonAdd;
