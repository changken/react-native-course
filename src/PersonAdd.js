import React, { useState, useEffect } from 'react';
import {
  Modal,
  TextInput,
  Button,
  View,
  StyleSheet,
  Alert,
} from 'react-native';
import axios from 'axios';

import { axios_config, base_url } from './config';

function PersonAdd(props) {
  const [name, setName] = useState('');
  const [cityName, setCityName] = useState('');
  const [age, setAge] = useState(0);

  useEffect(() => {
    setName(props.person.Name);
    setCityName(props.person.CityName);
    setAge(props.person.Age + '');
  }, [props.id]);

  const sendData = async () => {
    let cityArray = [];
    if (cityName.includes(',')) {
      cityArray = cityName.split(',');
    } else {
      cityArray = [...cityArray, cityName];
    }

    let url = props.id ? base_url + `/${props.id}` : base_url;

    const newPerson = props.id
      ? {
          fields: {
            Name: name,
            City: cityArray,
            Age: parseInt(age),
          },
          typecast: true,
        }
      : {
          fields: {
            Name: name,
            City: cityArray,
            Age: parseInt(age),
          },
          typecast: true,
        };

    try {
      const result = props.id
        ? await axios.put(url, newPerson, axios_config)
        : await axios.post(url, newPerson, axios_config);
      console.log(result);
      props.toggleModalVisible();
    } catch (error) {
      console.log(error);
    }
  };

  const deletePopup = () => {
    Alert.alert(`您確定要刪除${props.person.Name}?`, '此動作不可逆唷!', [
      { text: '刪除', onPress: deleteData },
      { text: '我再考慮看看', onPress: () => {} },
    ]);
  };

  const deleteData = async () => {
    const url = base_url + `/${props.id}`;
    try {
      const result = await axios.delete(url, axios_config);
      console.log(result);
      props.toggleModalVisible();
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <Modal visible={props.modalVisible}>
      <View style={styles.container}>
        <TextInput
          placeholder="大名"
          value={name}
          onChangeText={text => setName(text)}
          style={styles.textInput}
        />
        <TextInput
          placeholder="城市名稱(逗號分隔)"
          value={cityName}
          onChangeText={text => setCityName(text)}
          style={styles.textInput}
        />
        <TextInput
          placeholder="年齡"
          value={age + ''}
          onChangeText={text => setAge(text)}
          style={styles.textInput}
        />
        <Button title={props.id ? '編輯' : '新增'} onPress={sendData} />
        <Button title="刪除" onPress={props.id ? deletePopup : () => {}} />
        <Button title="Close" onPress={props.toggleModalVisible} />
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingLeft: 10,
    paddingRight: 10,
  },
  textInput: {
    fontSize: 20,
  },
  deleteBtn: {
    backgroundColor: '#ff0000',
    color: '#fff',
  },
});

export default PersonAdd;
