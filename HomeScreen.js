import React from 'react';
import { View, Text, Button } from 'react-native';
//import { useNavigation } from '@react-navigation/native';

function HomeScreen({
  route: {
    params: { count },
  },
}) {
  //const navigation = useNavigation();
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Home Screen {count}</Text>
      {/*<Button
        title="go to details"
        onPress={() => navigation.navigate('Details')}
      />*/}
    </View>
  );
}

export default HomeScreen;
